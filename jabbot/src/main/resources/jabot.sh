#!/bin/sh

JABOT_HOME=./

daemon_start(){
  exec jsvc  -cp "$JABOT_HOME/lib/*" -pidfile /tmp/jabbot.pid -debug -home /usr/lib/jvm/java-6-sun org.wanna.jabbot.Main
}

daemon_stop(){
  exec jsvc -stop -cp "$JABOT_HOME/lib/*" -pidfile /tmp/jabbot.pid -debug -home /usr/lib/jvm/java-6-sun org.wanna.jabbot.Main
}

case $1 in
start)
daemon_start
;;
stop)
daemon_stop
;;
*)
 echo "Usage: runner.sh {start|stop|restart|force-reload|status}" >&2
 exit 1
esac