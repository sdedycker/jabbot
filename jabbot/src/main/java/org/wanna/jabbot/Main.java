/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.wanna.jabbot;

import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.jivesoftware.smack.XMPPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author vincent
 * @since May 13, 2011 10:17:07 PM
 */
public class Main implements Daemon {
    private final Logger log = LoggerFactory.getLogger(Main.class);

    @Override
    public void init(DaemonContext daemonContext) throws Exception {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(
                "applicationContext.xml",
                "classpath*:jabbot-extension.xml"
        );
    }

    @Override
    public void start() throws Exception {
        Jabbot jabbot = Jabbot.getInstance();

        try {
            jabbot.connect();
            while(jabbot.isConnected()){
                Thread.sleep(1000);
            }
        } catch (XMPPException e) {
            log.error("error occured in jabbot",e);
        }catch(InterruptedException e){
            log.error("error occured in jabbot",e);
        }

    }

    @Override
    public void stop() throws Exception {
    }

    @Override
    public void destroy() {
    }

    public static void main(String[] args) throws Exception{
        Main main = new Main();
        main.init(null);
        main.start();
    }
}
