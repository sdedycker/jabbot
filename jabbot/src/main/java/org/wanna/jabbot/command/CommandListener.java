/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.wanna.jabbot.command;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wanna.jabbot.Jabbot;
import org.wanna.jabbot.command.parser.CommandParser;
import org.wanna.jabbot.command.parser.DefaultCommandParser;
import org.wanna.jabbot.command.parser.ParsedCommand;
import org.wanna.jabbot.command.response.CommandResponse;
import org.wanna.jabbot.command.response.ResponseType;

/**
 * @author vincent
 * @since May 28, 2011 11:32:56 AM
 */
public class CommandListener implements PacketListener {
    private Logger log = LoggerFactory.getLogger(CommandListener.class);

    private CommandParser commandParser = new DefaultCommandParser();

    @Override
    public void processPacket(Packet packet) {
        if(packet instanceof Message){
            Message message = (Message)packet;
            try {
                ParsedCommand parsed = commandParser.parse(message.getBody());
                Command command = CommandFactory.getInstance().create(parsed.getCommandName());
                CommandResponse response = command.execute(message);
                if(response.getType().equals(ResponseType.GROUPCHAT)){
                    try {
                        Jabbot.getInstance().getChatroom(response.getDestination()).sendMessage(response.getBody());
                    } catch (XMPPException e) {
                        log.warn("unable to send message to chatroom",e);
                    }
                }else{
                    try {
                        Jabbot.getInstance().getChat(response.getDestination()).sendMessage(response.getBody());
                    } catch (XMPPException e) {
                        log.warn("unable to send message to chat",e);
                    }
                }
            } catch (CommandNotFoundException e) {
                log.info("error processing incoming packet: {}",e.getMessage());
            } catch (ExecutionException e) {
                log.error("error executing command ",e);
            }
        }
    }

    public void setCommandParser(CommandParser commandParser) {
        this.commandParser = commandParser;
    }
}
