/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.wanna.jabbot.command.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * @author vincent
 * @since May 28, 2011 1:10:03 PM
 */
public class DefaultCommandParser implements CommandParser{
    private Logger log = LoggerFactory.getLogger(DefaultCommandParser.class);
    private String commandPrefix = "!";

    public ParsedCommand parse(String message){
        ParsedCommand parsed = new ParsedCommand();

        String command;
        if(message.contains(" ")){
            command = message.substring(commandPrefix.length(), message.indexOf(" "));
        }else{
            command = message.substring(commandPrefix.length());
        }

        parsed.setCommandName(command);
        StringTokenizer tokenizer = new StringTokenizer(message.substring((commandPrefix+command).length())," ");
        Set<String> args = new HashSet<String>();
        while(tokenizer.hasMoreTokens()){
            String token = tokenizer.nextToken();
            token = clearArgument(token);
            log.debug("one argument as been found '{}'",token);
            args.add(token);
        }
        parsed.setArgs(args.toArray(new String[args.size()]));
        return parsed;
    }

    private String clearArgument(String arg){
        return arg.trim();
    }

    public String getCommandPrefix() {
        return commandPrefix;
    }

    public void setCommandPrefix(String commandPrefix) {
        this.commandPrefix = commandPrefix;
    }
}
