/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.wanna.jabbot.scheduling;

import org.jivesoftware.smack.XMPPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wanna.jabbot.Jabbot;

/**
 * @author vincent
 * @since May 29, 2011 11:58:04 AM
 */
public class ExampleTask {
    private Logger log = LoggerFactory.getLogger(ExampleTask.class);

    public void testSayHello() throws XMPPException {
        log.debug("say hello activated");
        Jabbot.getInstance().getChatroom("tech-lounge@conference.voxbone.com").sendMessage("shut up Sander");
    }
}
