/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.wanna.jabbot;

import org.apache.commons.io.IOUtils;
import org.jivesoftware.smack.*;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.packet.VCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.io.InputStreamSource;
import org.wanna.jabbot.room.Chatroom;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * @author vincent
 * @since May 14, 2011 9:35:10 AM
 */
public class Jabbot {
    private final Logger log = LoggerFactory.getLogger(Jabbot.class);

    private static Jabbot _instance = new Jabbot();
    private List<String> messageQueue;
    private String username,password;
    private XMPPConnection connection;
    private Hashtable<String, Chatroom> chatrooms;

    private InputStreamSource avatar;

    private PacketListener commandListener;
    private PacketFilter commandFilter;

    public static Jabbot getInstance(){
        return _instance;
    }

    private Jabbot(){
        messageQueue = new ArrayList<String>();
    }
    
    public List<String> getMessageQueue(){
        return messageQueue;
    }

    public void connect() throws XMPPException{
        SmackConfiguration.setPacketReplyTimeout(100000);
        connection.connect();
        connection.login(username,password);
        for (Chatroom chatroom : chatrooms.values()) {
            chatroom.init(connection);
        }
        connection.addPacketListener(commandListener,commandFilter);
        /*
        From Torrey ( to force presence refresh ) 
		StreamElement vCard = _Conn.getDataFactory().createElementNode(new NSI("x", "vcard-temp:x:update"));
		StreamElement photo = vCard.addElement("photo");
		photo.addText(iconHash);

		p.add(vCard);
         */
        try {
            if(avatar != null){
                log.debug("avatar found, creating vcard");
                VCard vCard = new VCard();
                vCard.load(connection);
                byte[] bytes = IOUtils.toByteArray(avatar.getInputStream());
                String encodedImage = StringUtils.encodeBase64(bytes);
                vCard.setAvatar(bytes,encodedImage);
                vCard.setEncodedImage(encodedImage);
                vCard.setField("PHOTO", "<TYPE>image/jpg</TYPE><BINVAL>"
                    + encodedImage + "</BINVAL>", true);
                vCard.save(connection);
            }else{
                log.debug("no avatar source set");
            }
        } catch (IOException e) {
            log.error("error while setting avatar");
        }
    }

    public boolean isConnected(){
        return connection != null && connection.isConnected();
    }

    @Required
    public void setConnection(XMPPConnection connection) {
        if(this.connection != null && this.connection.isConnected()){
            connection.disconnect();
        }
        this.connection = connection;
    }

    @Required
    public void setUsername(String username) {
        this.username = username;
    }

    @Required
    public void setPassword(String password) {
        this.password = password;
    }

    @Required
    public void setCommandListener(PacketListener commandListener) {
        this.commandListener = commandListener;
    }

    @Required
    public void setCommandFilter(PacketFilter commandFilter) {
        this.commandFilter = commandFilter;
    }

    public Chatroom getChatroom(String chatroom) {
        return chatrooms.get(chatroom);
    }

    public Chat getChat(String thread){
        return connection.getChatManager().getThreadChat(thread);
    }

    public void setChatrooms(Hashtable<String, Chatroom> chatrooms) {
        this.chatrooms = chatrooms;
    }

    public InputStreamSource getAvatar() {
        return avatar;
    }

    public void setAvatar(InputStreamSource avatar) {
        this.avatar = avatar;
    }
}
