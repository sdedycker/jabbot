/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.wanna.jabbot.command.parser;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author vincent
 * @since May 28, 2011 1:29:10 PM
 */
public class DefaultCommandParserTest {
    private CommandParser parser = new DefaultCommandParser();
    private String commandName = "testCommand";
    private String[] args = {" arg1 "," arg2 ","arg3 "};

    private String command, commandWithArgs;

    @Before
    public void before(){
        command = parser.getCommandPrefix()+commandName;
        commandWithArgs = command;
        for (String arg : args) {
            commandWithArgs += " "+arg;
        }
    }

    @Test
    public void parseBasicCommand(){
        ParsedCommand parsed = parser.parse(command);
        Assert.assertNotNull("parsed message is null",parsed);
        Assert.assertEquals(commandName,parsed.getCommandName());
    }

    @Test
    public void parseBasicCommandWithSpace(){
        ParsedCommand parsed = parser.parse(command+" ");
        Assert.assertNotNull("parsed message is null",parsed);
        Assert.assertEquals(commandName,parsed.getCommandName());
    }

    @Test
    public void parseCommandWithArgs(){
        ParsedCommand parsed = parser.parse(commandWithArgs);
        Assert.assertNotNull("parsed message is null",parsed);
        Assert.assertEquals(commandName,parsed.getCommandName());
        Assert.assertNotNull("parsed arg list shouldn't be null",parsed.getArgs());
        Assert.assertEquals(args.length,parsed.getArgs().length);
    }
}
