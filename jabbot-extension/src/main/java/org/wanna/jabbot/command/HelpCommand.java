/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.wanna.jabbot.command;

import org.jivesoftware.smack.packet.Message;
import org.wanna.jabbot.command.response.CommandResponse;
import org.wanna.jabbot.command.response.ResponseType;

import java.util.Set;

/**
 * @author vincent
 * @since May 28, 2011 2:03:36 PM
 */
public class HelpCommand extends AbstractCommand{
    public HelpCommand(String name) {
        super(name);
    }

    public CommandResponse execute(Message message) throws ExecutionException{
        String sender = super.getReplyTo(message);
        Set<String> availableCommands = CommandFactory.getInstance().getAvailableCommands();
        StringBuilder sb = new StringBuilder();
        sb.append("Below is the list of available commands.\n");
        for (String availableCommand : availableCommands) {
            sb.append(availableCommand).append("\n");
        }

        CommandResponse response = super.createResponse();
        response.setBody(sb.toString());
        if(message.getType().equals(Message.Type.groupchat)){
            response.setType(ResponseType.GROUPCHAT);
            response.setDestination(sender);
        }else if(message.getType().equals(Message.Type.chat)){
            response.setType(ResponseType.CHAT);
            response.setDestination(message.getThread());
        }

        return response;

    }
}
