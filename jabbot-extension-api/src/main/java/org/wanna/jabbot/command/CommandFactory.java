/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.wanna.jabbot.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.Set;

/**
 * @author vincent
 * @since May 28, 2011 11:59:45 AM
 */
public class CommandFactory {
    Logger log = LoggerFactory.getLogger(this.getClass());

    private final static CommandFactory _instance = new CommandFactory();
    private LinkedHashMap<String,Command> commands = new LinkedHashMap<String, Command>();

    public void setCommands(LinkedHashMap<String, Command> commands) {
        this.commands = commands;
    }

    public static CommandFactory getInstance(){
        return _instance;
    }

    public Command create(String name) throws CommandNotFoundException{
        if(!commands.containsKey(name)){
            throw new CommandNotFoundException(name);
        }
        return commands.get(name);
    }

    public void register(Command command){
        log.info("registering command '{}'",command.getName());
        commands.put(command.getName(),command);
    }

    public void remove(String commandName){
        log.info("remove '{}' from command registry",commandName);
        commands.remove(commandName);
    }

    public Set<String> getAvailableCommands(){
        return commands.keySet();
    }
}
