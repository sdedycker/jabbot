/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.wanna.jabbot.command;

import org.jivesoftware.smack.packet.Message;
import org.wanna.jabbot.command.response.CommandResponse;
import org.wanna.jabbot.command.response.DefaultCommandResponse;

/**
 * @author vincent
 * @since May 28, 2011 12:57:04 PM
 */
public abstract class AbstractCommand implements Command{
    private final String name;
    private Message message;

    protected AbstractCommand(String name) {
        this.name = name;
    }

    public final String getName() {
        return name;
    }

    public final void init(){
        CommandFactory.getInstance().register(this);
    }
/*
    public final void execute(Message message) {
        this.message = message;
    }

    protected abstract void executeInternal(Message message);
*/
    
    /**
     * Determine if the answer should go to a MultiUserChat or to an User
     * @param message Message from which the destination will be extracted
     * @return Destination
     */
    public final String getReplyTo(Message message){
        String sender;
        if(message.getType().equals(Message.Type.groupchat)){
            sender = message.getFrom().substring(0,message.getFrom().indexOf("/"));
            System.out.println("sender is groupchat "+sender);
        }else{
            sender = message.getFrom();
        }
        return sender;
    }

    public final CommandResponse createResponse(){
        return new DefaultCommandResponse();
    }
}
