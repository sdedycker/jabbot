/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.wanna.jabbot.command;

import org.jivesoftware.smack.packet.Message;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.wanna.jabbot.command.response.CommandResponse;

/**
 * @author vincent
 * @since May 28, 2011 12:39:40 PM
 */

public class CommandFactoryTest{
    CommandFactory factory = CommandFactory.getInstance();

    private final String commandName = "test";
    private Command command;

    @Before
    public void before(){
        command = new Command() {
            @Override
            public CommandResponse execute(Message message) {
                return null;
            }

            @Override
            public String getName() {
                return commandName;
            }
        };

        factory.register(command);
    }

    @After
    public void after(){
        factory.remove(commandName);
    }

    @Test
    public void createValidCommand() throws CommandNotFoundException {
        Command command = factory.create(commandName);
        Assert.assertNotNull("created command is null",command);
        Assert.assertEquals(commandName,command.getName());
    }

    @Test(expected = CommandNotFoundException.class)
    public void unregister() throws CommandNotFoundException{
        factory.remove(command.getName());
        factory.create(commandName);
    }

    @Test(expected = CommandNotFoundException.class)
    public void createInvalidCommand() throws CommandNotFoundException{
        factory.create("this_should_be_an_invalid_command");
    }
}
